package com.revolut.testapp.data

import com.revolut.testapp.RxImmediateSchedulerRule
import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.injection.component.DaggerAndroidTestViewModelInjector
import com.revolut.testapp.injection.module.RepositoryModule
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject


class DefaultCurrencyRepositoryAndroidTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()


    @Inject
    lateinit var currencyRepository: CurrencyRepository


    @Before
    fun setUp() {

        val component = DaggerAndroidTestViewModelInjector.builder()
            .repositoryModule(RepositoryModule())
            .build()
        component.inject(this)

    }


    @Test
    fun getLatest_EUR() {

        Assert.assertNotNull(currencyRepository)

        val result = currencyRepository.getLatest("EUR")
        val testObserver = TestObserver<CurrencyRates>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val listResult = testObserver.values()[0]
        MatcherAssert.assertThat(listResult?.base, CoreMatchers.`is`("EUR"))
        MatcherAssert.assertThat(listResult?.rates?.size, CoreMatchers.not(0))

    }


    @Test
    fun getLatest_AUD() {

        Assert.assertNotNull(currencyRepository)

        val result = currencyRepository.getLatest("AUD")
        val testObserver = TestObserver<CurrencyRates>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val listResult = testObserver.values()[0]
        MatcherAssert.assertThat(listResult?.base, CoreMatchers.`is`("AUD"))
        MatcherAssert.assertThat(listResult?.rates?.size, CoreMatchers.not(0))

    }

}