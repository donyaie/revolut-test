package com.revolut.testapp.data

import com.revolut.testapp.data.model.CurrencyRates
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.random.Random

class FakeCurrencyRepository : CurrencyRepository {


    override fun getLatest(base: String): Observable<CurrencyRates> {

        return Observable.create<CurrencyRates> { emitter ->
            emitter.onNext(getLatestData(base))
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    }


    private fun getLatestData(base: String): CurrencyRates {


        val rateMap =
            mutableMapOf(
                "EUR" to getVale(),
                "AUD" to getVale(),
                "BGN" to getVale(),
                "BRL" to getVale()
            )

        if (rateMap.containsKey(base))
            rateMap.remove(base)

        return CurrencyRates(base, "2018-09-06", rateMap)
    }

    private fun getVale(): Double {
        return Random.nextDouble(0.8, 2.0)
    }

}
