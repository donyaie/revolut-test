package com.revolut.testapp.injection.module

import com.revolut.testapp.data.CurrencyRepository
import com.revolut.testapp.data.FakeCurrencyRepository
import com.revolut.testapp.data.network.CurrencyApi
import io.mockk.mockk
import okhttp3.OkHttpClient
import retrofit2.Retrofit


class AndroidTestRepositoryModule : RepositoryModule() {

    override fun provideCurrencyApi(retrofit: Retrofit): CurrencyApi = mockk()

    override fun provideCurrencyRepository(api: CurrencyApi): CurrencyRepository =
        FakeCurrencyRepository()

    override fun provideRetrofitInterface(client: OkHttpClient): Retrofit = mockk()

    override fun provideOkHttp(): OkHttpClient = mockk()

}