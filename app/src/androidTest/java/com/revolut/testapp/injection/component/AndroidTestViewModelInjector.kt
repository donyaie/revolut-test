package com.revolut.testapp.injection.component

import com.revolut.testapp.data.DefaultCurrencyRepositoryAndroidTest
import com.revolut.testapp.injection.module.RepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(RepositoryModule::class)])
interface AndroidTestViewModelInjector : ViewModelInjector {
    fun inject(repository: DefaultCurrencyRepositoryAndroidTest)


}