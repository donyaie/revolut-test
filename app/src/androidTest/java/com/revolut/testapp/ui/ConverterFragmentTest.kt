package com.revolut.testapp.ui

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.revolut.testapp.R
import com.revolut.testapp.RxImmediateSchedulerRule
import com.revolut.testapp.ServiceLocator
import com.revolut.testapp.injection.module.AndroidTestRepositoryModule
import com.revolut.testapp.testUtils.OrientationChangeAction.Companion.orientationLandscape
import com.revolut.testapp.testUtils.TestUtils.withRecyclerView
import com.revolut.testapp.ui.converter.ConverterFragment
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@MediumTest
@RunWith(AndroidJUnit4::class)
class ConverterFragmentTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Before
    fun setUp() {
        ServiceLocator.repositoryModule = AndroidTestRepositoryModule()
    }

    @Test
    fun converterFragment_DisplayedInUi() {

        // WHEN - Details fragment launched to display task
        val bundle = Bundle()
        launchFragmentInContainer<ConverterFragment>(bundle, R.style.AppTheme)



        onView(withId(R.id.recycler)).check(matches(isDisplayed()))



        Thread.sleep(1000)



        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(0, R.id.title)
        )
            .check(matches(withText("EUR")))

        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(1, R.id.title)
        )
            .perform(click())

        Thread.sleep(1000)


        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(0, R.id.title)
        )
            .check(matches(withText("AUD")))


        Thread.sleep(1000)

        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(0, R.id.input)
        )
            .perform(replaceText("10"))


        Thread.sleep(1000)

        onView(isRoot()).perform(orientationLandscape())

        Thread.sleep(1000)

        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(0, R.id.title)
        )
            .check(matches(withText("AUD")))

        onView(
            withRecyclerView(R.id.recycler)
                .atPositionOnView(0, R.id.input)
        )
            .check(matches(withText("10")))


    }
}