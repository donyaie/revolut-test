package com.revolut.testapp.base

import androidx.lifecycle.ViewModel
import com.revolut.testapp.ServiceLocator
import com.revolut.testapp.injection.component.DaggerViewModelInjector
import com.revolut.testapp.injection.component.ViewModelInjector
import com.revolut.testapp.ui.converter.ConverterViewModel

abstract class BaseViewModel : ViewModel() {

    private   var injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .repositoryModule(ServiceLocator.provideRepositoryModule())
        .build()


    init {

        inject()
    }

    private fun inject() {
        when (this) {
            is ConverterViewModel -> injector.inject(this)

        }
    }


}