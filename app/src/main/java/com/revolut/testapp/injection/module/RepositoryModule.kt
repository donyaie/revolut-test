package com.revolut.testapp.injection.module

import com.revolut.testapp.BuildConfig
import com.revolut.testapp.data.CurrencyRepository
import com.revolut.testapp.data.DefaultCurrencyRepository
import com.revolut.testapp.data.network.CurrencyApi
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Module providing Repositories
 */
@Module
open class RepositoryModule {

    @Provides
    @Singleton
    open fun provideCurrencyApi(retrofit: Retrofit): CurrencyApi {
        return retrofit.create(CurrencyApi::class.java)
    }

    @Provides
    @Singleton
    open fun provideCurrencyRepository(api: CurrencyApi): CurrencyRepository {
        return DefaultCurrencyRepository(api)
    }


    @Provides
    @Singleton
    open fun provideRetrofitInterface(client:OkHttpClient): Retrofit {


        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }



    @Provides
    @Singleton
    open fun provideOkHttp(): OkHttpClient {
        val clientBuilder =OkHttpClient.Builder()
        //okHttp logger for Debug Mode
        clientBuilder.addInterceptor(HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        })

       return clientBuilder.build()

    }

}