package com.revolut.testapp.injection.component

import com.revolut.testapp.injection.module.RepositoryModule
import com.revolut.testapp.ui.converter.ConverterViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for viewModel.
 */
@Singleton
@Component(modules = [(RepositoryModule::class)])
interface ViewModelInjector {


    fun inject(viewModel: ConverterViewModel)


    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun repositoryModule(repositoryModule: RepositoryModule): Builder
    }

}