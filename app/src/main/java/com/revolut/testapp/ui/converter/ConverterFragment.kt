package com.revolut.testapp.ui.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolut.testapp.R
import com.revolut.testapp.base.BaseFragment
import com.revolut.testapp.utils.SpeedyLinearLayoutManager

class ConverterFragment : BaseFragment() {

    private lateinit var binding: com.revolut.testapp.databinding.FragmentConverterBinding
    private lateinit var viewModel: ConverterViewModel


    lateinit var adapter: ConverterAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_converter, container, false)
        viewModel = ViewModelProviders.of(this).get(ConverterViewModel::class.java)
        binding.viewModel = viewModel


        binding.recycler.layoutManager =
            SpeedyLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        viewModel.getErrorMessage().observe(viewLifecycleOwner, Observer { errorMessage ->
            if (errorMessage != null) Toast.makeText(
                requireContext(),
                errorMessage,
                Toast.LENGTH_LONG
            ).show()
        })
        viewModel.getScrollToTop().observe(viewLifecycleOwner, Observer {
            binding.recycler.post { binding.recycler.scrollToPosition(0) }
        })

        viewModel.getNotifyItemChanged().observe(viewLifecycleOwner, Observer {
            adapter.notifyItemChanged(it)
        })
        viewModel.getNotifyItemInserted().observe(viewLifecycleOwner, Observer {
            adapter.notifyItemInserted(it)
        })
        viewModel.getNotifyItemMovedTop().observe(viewLifecycleOwner, Observer {
            adapter.notifyItemMoved(it, 0)
        })


        adapter = ConverterAdapter(
            viewModel.getDataList(),
            viewModel
        )
        adapter.notifyDataSetChanged()

        binding.recycler.adapter = adapter

        return binding.root
    }


    override fun onStart() {
        super.onStart()
        viewModel.startUpdateData()
    }


    override fun onStop() {
        super.onStop()
        viewModel.stopUpdateData()
    }


}