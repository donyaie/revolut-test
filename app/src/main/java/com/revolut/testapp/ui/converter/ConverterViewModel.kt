package com.revolut.testapp.ui.converter

import androidx.lifecycle.MutableLiveData
import com.revolut.testapp.base.BaseViewModel
import com.revolut.testapp.data.CurrencyRepository
import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.data.model.toRateList
import com.revolut.testapp.utils.AppDebug
import com.revolut.testapp.utils.NetworkError
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class ConverterViewModel : BaseViewModel(), ConverterAdapter.AdapterListener {


    @Inject
    lateinit var currencyRepository: CurrencyRepository

    var baseCurrency = "EUR"
    var baseRate = 1.0


    private var timer: Timer? = null


    private val errorMessage: MutableLiveData<String> = MutableLiveData()
    fun getErrorMessage(): MutableLiveData<String> {
        return errorMessage

    }

    private val scrollToTop: MutableLiveData<Boolean> = MutableLiveData()
    fun getScrollToTop(): MutableLiveData<String> {
        return errorMessage

    }

    private val notifyItemInserted: MutableLiveData<Int> = MutableLiveData()
    fun getNotifyItemInserted(): MutableLiveData<Int> {
        return notifyItemInserted

    }

    private val notifyItemChanged: MutableLiveData<Int> = MutableLiveData()
    fun getNotifyItemChanged(): MutableLiveData<Int> {
        return notifyItemChanged

    }

    private val notifyItemMovedTop: MutableLiveData<Int> = MutableLiveData()
    fun getNotifyItemMovedTop(): MutableLiveData<Int> {
        return notifyItemMovedTop

    }


    private val dataList = ArrayList<CurrencyRates.Rate>()

    fun getDataList() = dataList

//    val adapter = CurrencyAdapter(dataList, this)


    private var subscription: Disposable? = null

    fun updateList() {

        subscription?.dispose()
        subscription = currencyRepository.getLatest(baseCurrency)

            .doOnSubscribe { }
            .doOnTerminate { }
            .subscribe({ result ->


                updateList(result)


            }, {

                NetworkError(it).apply {
                    if (message.isNotBlank() && message.compareTo(errorMessage.value ?: "") != 0)
                        errorMessage.postValue(message)
                }


            })

    }


    fun updateList(data: CurrencyRates) {

        baseCurrency = data.base

        dataList.filter { l -> l.key.compareTo(baseCurrency) == 0 }.apply {

            if (this.isEmpty()) {
                dataList.add(0, CurrencyRates.Rate(baseCurrency, baseRate)
                    .apply {
                        isBase = true
                        valueMutable?.postValue(baseRate)
                    }
                )

                notifyItemInserted.postValue(0)
            }

        }

        data.rates.toRateList().forEach { value ->
            updateValue(value)
        }


    }


    private fun updateValue(value: CurrencyRates.Rate) {


        dataList.filter { l -> l.key.compareTo(value.key) == 0 }.apply {

            if (this.isNotEmpty()) {
                if (!this[0].isBase) {
                    this[0].rate = value.rate
                    this[0].value = value.rate * baseRate
                    this[0].valueMutable?.postValue(value.rate * baseRate)
                        ?: run {
                            notifyItemChanged.postValue(dataList.indexOf(this[0]))
                        }
                }

            } else {

                dataList.add(value.apply { this.value = this.rate * baseRate })
                notifyItemInserted.postValue(dataList.indexOf(value))


            }


        }


    }


    override fun onBaseChanged(rate: CurrencyRates.Rate) {


        //Base flag
        dataList.forEach { it.isBase = false }
        rate.isBase = true


        //setCurrent Base
        baseCurrency = rate.key


        //Change multipel value

        AppDebug.log("onBaseRateChanged  change Base", baseRate.toString())
        onBaseRateChanged(rate.value)


        //Move base to Top
        val index = dataList.indexOf(rate)
        dataList.removeAt(index)
        dataList.add(0, rate)


        notifyItemMovedTop.postValue(index)


    }

    override fun onBaseRateChanged(base: Double) {
        baseRate = base

        dataList.filter { l -> l.key.compareTo(baseCurrency) != 0 }.forEach {
            it.valueMutable?.postValue(it.rate * baseRate)

        }
//
//        AppDebug.log("onBaseRateChanged", base.toString())
    }

    override fun scrollToTop() {
        scrollToTop.postValue(true)
    }


    fun startUpdateData() {

        timer?.cancel()
        timer = Timer()
        timer?.schedule(delay = 0, period = 1000) {
            updateList()
        }
    }

    fun stopUpdateData() {
        timer?.cancel()
        timer = null

    }


    override fun onCleared() {
        super.onCleared()
        stopUpdateData()
        subscription?.dispose()


    }

}