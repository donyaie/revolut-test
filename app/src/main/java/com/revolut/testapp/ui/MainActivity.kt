package com.revolut.testapp.ui

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.revolut.testapp.R
import com.revolut.testapp.base.BaseActivity
import com.revolut.testapp.ui.converter.ConverterFragment

class MainActivity : BaseActivity() {

    private lateinit var binding: com.revolut.testapp.databinding.ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, ConverterFragment())
            .commit()


    }

}