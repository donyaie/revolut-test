package com.revolut.testapp.ui.converter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.mynameismidori.currencypicker.ExtendedCurrency
import com.revolut.testapp.R
import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.databinding.AdapterConverterBinding
import com.revolut.testapp.utils.AppDebug
import com.revolut.testapp.utils.toFormatDouble
import io.reactivex.disposables.Disposable


class ConverterAdapter(
    private val dataList: ArrayList<CurrencyRates.Rate>, val listener: AdapterListener? = null
) :
    RecyclerView.Adapter<ConverterAdapter.ViewHolder>() {

    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: AdapterConverterBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.adapter_converter,
            parent,
            false
        )

        registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                listener?.scrollToTop()
            }

        })



        return ViewHolder(
            binding,
            listener
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val animation: Animation = AnimationUtils.loadAnimation(
            holder.itemView.context,
            if (position > lastPosition) R.anim.adapter_up_from_bottom else R.anim.adapter_down_from_top
        )
        holder.itemView.startAnimation(animation)
        lastPosition = position


        holder.bind(dataList[position])
    }


    override fun getItemCount() = dataList.size


    override fun onViewRecycled(holder: ViewHolder) {
        holder.onViewRecycled()
        super.onViewRecycled(holder)
    }


    class ViewHolder(
        private val binding: AdapterConverterBinding,
        private val listener: AdapterListener?
    ) : RecyclerView.ViewHolder(binding.root) {

        private val title = MutableLiveData<String>()
        private val description = MutableLiveData<String>()
        private val value = MutableLiveData<Double>()
        private val icon = MutableLiveData<Int>()

        fun getTitle() = title
        fun getDescription() = description
        fun getValue() = value
        fun getIcon() = icon


        var disposable: Disposable? = null

        init {
            binding.viewHolder = this
        }

        lateinit var mData: CurrencyRates.Rate


        fun bind(data: CurrencyRates.Rate) {

            mData = data

            title.value = data.key

            //update new value
            value.value = data.value

            //register for update
            data.valueMutable = value


            ExtendedCurrency.getCurrencyByISO(data.key)?.let {
                icon.value = it.flag
                description.value = it.name
            }

            binding.root.setOnClickListener {

                binding.input.requestFocus()
            }

            binding.input.setOnFocusChangeListener { _, select ->
                if (select) {
                    listener?.onBaseChanged(data)
                }

            }


            disposable = RxTextView.textChanges(binding.input)
                .subscribe {

                    val value = it.toString().toFormatDouble() ?: 0.0
                    data.value = value
                    if (data.isBase) {
                        value.let {
                            AppDebug.log("onBaseRateChanged adapter", value.toString())

                            listener?.onBaseRateChanged(value)
                        }
                    }
                }


        }

        fun onViewRecycled() {
            mData.valueMutable = null
            binding.image.setImageResource(0)
            disposable?.dispose()
        }


    }


    interface AdapterListener {
        fun onBaseChanged(rate: CurrencyRates.Rate)
        fun onBaseRateChanged(base: Double)
        fun scrollToTop()
    }

    companion object {
        var counter = 1
    }


}