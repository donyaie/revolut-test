package com.revolut.testapp.utils

import android.content.ContextWrapper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner

fun View.getParentActivity(): AppCompatActivity? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }

        context = context.baseContext
    }
    return null
}


fun View.getLifecycleOwner(): LifecycleOwner? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is LifecycleOwner) {
            return context
        }

        context = context.baseContext
    }
    return null
}










