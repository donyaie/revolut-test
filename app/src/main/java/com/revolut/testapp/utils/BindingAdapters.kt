package com.revolut.testapp.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {

    val context = view.getLifecycleOwner()
    if (context != null && text != null) {
        text.observe(context, Observer { value ->
            view.text = value ?: ""
        })
    }
}

@BindingAdapter("mutableTextNumber")
fun setMutableTextNumber(view: TextView, text: MutableLiveData<Double>?) {

    val context = view.getLifecycleOwner()
    if (context != null && text != null) {

        text.observe(context, Observer { value ->
            view.text = value?.toFormatString()
        })
    }
}


@BindingAdapter("mutableImageSrc")
fun setMutableImageSrc(view: ImageView, src: MutableLiveData<Int>?) {
    val context = view.getLifecycleOwner()
    if (context != null && src != null) {
        src.observe(context, Observer { value -> view.setImageResource(value) })
    }
}