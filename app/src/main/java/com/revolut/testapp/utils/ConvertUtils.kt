package com.revolut.testapp.utils

import java.text.NumberFormat


fun String.toFormatDouble(): Double? {

    var value: Double? = null

    try {
        value =
            if (this.isBlank()) null else (NumberFormat.getNumberInstance().parse(this)?.toDouble())
    } catch (ex: Exception) {

    }
    return value

}


fun Double.toFormatString(): String {

    var value = "0"

    try {
        value = NumberFormat.getNumberInstance().format(this) ?: "0"
    } catch (ex: Exception) {

    }
    return value

}
