package com.revolut.testapp.utils

import android.util.Log
import com.revolut.testapp.BuildConfig


object AppDebug {

    fun log(TAG: String, Message: String?) {


        Message?.let {
            if (BuildConfig.DEBUG)
                Log.d(TAG, it)
        }
    }

    fun log(TAG: String, ex: Exception?) {
        ex?.let {
            log(TAG, "Exception ", ex)
        }
    }

    fun log(TAG: String, Message: String?, ex: Exception?) {

        var message1 = Message ?: ""

        if (ex != null)
            message1 = message1 + " --> " + ex.message

        if (BuildConfig.DEBUG)
            Log.e(TAG, message1)


        ex?.let {

            if (BuildConfig.DEBUG)
                it.printStackTrace()
        }
    }

}
