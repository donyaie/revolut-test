package com.revolut.testapp.data

import com.revolut.testapp.data.model.CurrencyRates
import io.reactivex.Observable


interface CurrencyRepository {
    fun getLatest(base: String): Observable<CurrencyRates>
}
