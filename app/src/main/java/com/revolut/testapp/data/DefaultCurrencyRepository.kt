package com.revolut.testapp.data

import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.data.network.CurrencyApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class DefaultCurrencyRepository(private val api: CurrencyApi) : CurrencyRepository {


    override fun getLatest(base: String): Observable<CurrencyRates> {
        return api.getLatest(base)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }


}