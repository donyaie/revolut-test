package com.revolut.testapp.data.network

import com.revolut.testapp.data.model.CurrencyRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface CurrencyApi {
    @GET("/latest")
    @Headers("Content-Type:application/json; charset=UTF-8")
    fun getLatest(@Query("base") Base: String): Observable<CurrencyRates>
}