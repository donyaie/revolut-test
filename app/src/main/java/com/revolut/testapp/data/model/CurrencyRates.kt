package com.revolut.testapp.data.model

import androidx.lifecycle.MutableLiveData
import com.google.gson.annotations.SerializedName


data class CurrencyRates(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: String,
    @SerializedName("rates") val rates: Map<String, Double>
) {

    data class Rate(val key: String, var rate: Double) {

        var valueMutable: MutableLiveData<Double>? = null
        var value: Double = rate
        var isBase: Boolean = false
    }


}


fun Map<String, Double>.toRateList(): ArrayList<CurrencyRates.Rate> {

    val result: ArrayList<CurrencyRates.Rate> = ArrayList(this.size)
    for (id in this.keys) {
        val range = CurrencyRates.Rate(id, this[id] ?: 0.0)
        result.add(range)
    }
    return result

}


