package com.revolut.testapp

import androidx.annotation.VisibleForTesting
import com.revolut.testapp.injection.module.RepositoryModule

object ServiceLocator {

    @Volatile
    var repositoryModule: RepositoryModule? = null
        @VisibleForTesting set


    fun provideRepositoryModule(): RepositoryModule {
        synchronized(this) {
            return repositoryModule ?: RepositoryModule()
        }
    }

}