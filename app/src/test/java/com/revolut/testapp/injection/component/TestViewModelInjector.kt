package com.revolut.testapp.injection.component

import com.revolut.testapp.data.DefaultCurrencyRepositoryTest
import com.revolut.testapp.injection.module.RepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(RepositoryModule::class)])
interface TestViewModelInjector : ViewModelInjector {

    fun inject(repository: DefaultCurrencyRepositoryTest)

}