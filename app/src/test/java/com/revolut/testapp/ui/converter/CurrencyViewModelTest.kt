package com.revolut.testapp.ui.converter

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.revolut.testapp.ServiceLocator
import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.injection.module.TestRepositoryModule
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class CurrencyViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    lateinit var converterViewModel: ConverterViewModel


    @Before
    fun setUp() {

        ServiceLocator.repositoryModule = TestRepositoryModule()

        converterViewModel = ConverterViewModel()


    }

    private fun getData(): CurrencyRates {

        val rateMap = mapOf("AUD" to 1.6232, "BGN" to 1.964, "BRL" to 4.8119)

        return CurrencyRates("EUR", "2018-09-06", rateMap)

    }


    @Test
    fun onBaseChanged() {


        Assert.assertNotNull(converterViewModel.currencyRepository)

        converterViewModel.updateList(getData())

        MatcherAssert.assertThat(converterViewModel.baseCurrency, CoreMatchers.`is`("EUR"))
        MatcherAssert.assertThat(converterViewModel.baseRate, CoreMatchers.`is`(1.0))


        val neBase = converterViewModel.getDataList()[2]

        converterViewModel.onBaseChanged(neBase)

        MatcherAssert.assertThat(
            converterViewModel.getDataList().indexOf(neBase),
            CoreMatchers.`is`(0)
        )

        MatcherAssert.assertThat(converterViewModel.baseCurrency, CoreMatchers.`is`("BGN"))
        MatcherAssert.assertThat(converterViewModel.baseRate, CoreMatchers.`is`(1.964))


    }


    @Test
    fun onBaseRateChanged_after_UpdateList() {

        converterViewModel.updateList(getData())

        MatcherAssert.assertThat(
            converterViewModel.getDataList()[1].value,
            CoreMatchers.`is`(1.6232)
        )

        converterViewModel.onBaseRateChanged(10.0)

        converterViewModel.updateList(getData())

        MatcherAssert.assertThat(
            converterViewModel.getDataList()[1].value,
            CoreMatchers.`is`(16.232)
        )

        MatcherAssert.assertThat(converterViewModel.baseRate, CoreMatchers.`is`(10.0))

    }


}