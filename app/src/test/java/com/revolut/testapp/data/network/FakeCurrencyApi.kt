package com.revolut.testapp.data.network

import com.revolut.testapp.data.model.CurrencyRates
import io.reactivex.Observable
import kotlin.random.Random

class FakeCurrencyApi : CurrencyApi {


    override fun getLatest(Base: String): Observable<CurrencyRates> {

        return Observable.create<CurrencyRates> { emitter ->
            emitter.onNext(getLatestData(Base))
        }
    }


    companion object {
        fun getLatestData(base: String): CurrencyRates {


            val rateMap =
                mutableMapOf(
                    "EUR" to getVale(),
                    "AUD" to getVale(),
                    "BGN" to getVale(),
                    "BRL" to getVale()
                )

            if (rateMap.containsKey(base))
                rateMap.remove(base)

            return CurrencyRates(base, "2018-09-06", rateMap)
        }

        fun getVale(): Double {
            return Random.nextDouble(0.8, 2.0)
        }
    }

}