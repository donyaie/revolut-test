package com.revolut.testapp.data

import com.revolut.testapp.data.model.CurrencyRates
import com.revolut.testapp.injection.component.DaggerTestViewModelInjector
import com.revolut.testapp.injection.module.TestRepositoryModule
import io.mockk.every
import io.reactivex.Observable
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import javax.inject.Inject


class DefaultCurrencyRepositoryTest {


    @Inject
    lateinit var currencyRepository: CurrencyRepository


    @Before
    fun setUp() {

        val component = DaggerTestViewModelInjector.builder()
            .repositoryModule(TestRepositoryModule())
            .build()
        component.inject(this)

    }


    @Test
    fun getLatest_EUR() {


        assertNotNull(currencyRepository)


        val rateMap = mapOf<String, Double>("AUD" to 1.6232, "BGN" to 1.964, "BRL" to 4.8119)

        val rates = CurrencyRates("EUR", "2018-09-06", rateMap)


        every { currencyRepository.getLatest("EUR") } returns Observable.just(rates)

        /* When */
        val result = currencyRepository.getLatest("EUR")

        /* Then */
        result
            .test()
            .assertValue(rates)

    }
}