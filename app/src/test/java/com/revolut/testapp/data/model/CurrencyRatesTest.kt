package com.revolut.testapp.data.model

import com.revolut.testapp.data.network.FakeCurrencyApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test

class CurrencyRatesTest {

    @Test
    fun toRateList() {

        val rates = FakeCurrencyApi.getLatestData("EUR")

        val ratesArrayList = rates.rates.toRateList()

        MatcherAssert.assertThat(ratesArrayList.size, CoreMatchers.`is`(3))
        MatcherAssert.assertThat(ratesArrayList[2].key, CoreMatchers.`is`("BRL"))
        MatcherAssert.assertThat(ratesArrayList[2].rate, CoreMatchers.`is`(rates.rates["BRL"]))

    }
}